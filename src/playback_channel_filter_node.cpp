#include "MusicBeatDetector/MusicBeatDetector/include/MusicBeatDetector/Utils/Data/PcmAudioFrameFormat.h"
#include "SocketServer/AudioFrameServer.h"
#include "playback_node.cpp"

#include <audio_utils/ChannelFilter.h>

int formatToNbits(PcmAudioFrameFormat format)
{
    if (format == PcmAudioFrameFormat::Signed8)
    {
        return 8;
    }
    else if (format == PcmAudioFrameFormat::Signed16)
    {
        return 16;
    }
    else if (format == PcmAudioFrameFormat::Signed32)
    {
        return 32;
    }
    else
    {
        throw std::runtime_error("Unsupported format");
    }
}

class PlaybackChannelFilterNode : public PlaybackNode
{
    ros::Subscriber m_channelFilterSub;
    std::vector<uint8_t> m_channelFilter = {};
    AudioFrameServer* m_testServer;
    ros::Publisher m_sssPub;
    PcmAudioFrameFormat m_format;

public:
    explicit PlaybackChannelFilterNode(PlaybackNodeConfiguration configuration, int port, string frame_id, int poolSize)
        : PlaybackNode(configuration)
    {
        m_channelFilterSub =
            m_nodeHandle.subscribe("channel_filter", 100, &PlaybackChannelFilterNode::channelFilterCallback, this);
        m_sssPub = m_nodeHandle.advertise<audio_utils::AudioFrame>("sss", 100);
        m_format = configuration.format;

        if (port > 0)
        {
            m_testServer = new AudioFrameServer(
                port,
                formatToNbits(configuration.format),
                configuration.channelCount,
                configuration.samplingFrequency,
                configuration.frameSampleCount,
                frame_id,
                poolSize);
            m_testServer->setCallback(PlaybackChannelFilterNode::serverAudioCallback, this);
            m_testServer->start();
        }
    }

    static void serverAudioCallback(audio_utils::AudioFramePtr message, void* userdata)
    {
        PlaybackChannelFilterNode* p = static_cast<PlaybackChannelFilterNode*>(userdata);

        p->audioCallback(message);
        p->sendSSS(message);
    }

    void sendSSS(audio_utils::AudioFramePtr message) { m_sssPub.publish(message); }

    void audioCallback(const audio_utils::AudioFramePtr& msg)
    {
        channelFilter(msg->data, msg->channel_count, m_channelFilter);

        PlaybackNode::audioCallback(msg);
    }

    void channelFilterCallback(const audio_utils::ChannelFilterPtr& msg) { m_channelFilter = msg->channel_filter; }

private:
    void channelFilter(std::vector<uint8_t>& audioData, uint32_t channel_count, std::vector<uint8_t>& channelFilter)
    {
        // For each channel from audioData not in channelFilter write zeroes
        for (int i = 0; i < channelFilter.size(); i++)
        {
            int channel = channelFilter.data()[i];
            if (channel < channel_count)
            {
                for (int j = channel; j < audioData.size(); j = j + (channel_count * (formatToNbits(m_format) / 8)))
                {
                    for (int k = 0; k < (formatToNbits(m_format) / 8); k++)
                    {
                        audioData.data()[j + k] = 0;
                    }
                }
            }
            else
            {
                ROS_WARN("Channel filter contains channels outside of audio frame");
            }
        }
    }
};

int main(int argc, char** argv)
{
    ros::init(argc, argv, "playback_channel_filter_node");

    ros::NodeHandle privateNodeHandle("~");

    PlaybackNodeConfiguration configuration;

    if (!privateNodeHandle.getParam("backend", configuration.backendString))
    {
        ROS_ERROR("The parameter backend must be alsa or pulse_audio.");
        return -1;
    }

    try
    {
        configuration.backend = PcmDevice::parseBackend(configuration.backendString);

        if (!privateNodeHandle.getParam("device", configuration.device))
        {
            ROS_ERROR("The parameter device is required.");
            return -1;
        }
        if (!privateNodeHandle.getParam("format", configuration.formatString))
        {
            ROS_ERROR("The parameter format is required.");
            return -1;
        }
        configuration.format = parseFormat(configuration.formatString);

        if (!privateNodeHandle.getParam("channel_count", configuration.channelCount))
        {
            ROS_ERROR("The parameter channel_count is required.");
            return -1;
        }
        if (!privateNodeHandle.getParam("sampling_frequency", configuration.samplingFrequency))
        {
            ROS_ERROR("The parameter sampling_frequency is required.");
            return -1;
        }
        if (!privateNodeHandle.getParam("frame_sample_count", configuration.frameSampleCount))
        {
            ROS_ERROR("The parameter frame_sample_count is required.");
            return -1;
        }
        if (!privateNodeHandle.getParam("latency_us", configuration.latencyUs))
        {
            ROS_ERROR("The parameter latency_us is required.");
            return -1;
        }

        bool channelMapFound = privateNodeHandle.getParam("channel_map", configuration.channelMap);
        if (channelMapFound && configuration.backend != PcmDevice::Backend::PulseAudio)
        {
            ROS_WARN("The parameter channel_map is only supported with the PulseAudio backend");
        }

        int port;
        if (!privateNodeHandle.getParam("sss_socket_port", port))
        {
            ROS_ERROR("The parameter sss_socket_port is required.");
            return -1;
        }
        string frame_id;
        if (!privateNodeHandle.getParam("sss_socket_frame_id", frame_id))
        {
            ROS_ERROR("The parameter sss_socket_frame_id is required.");
            return -1;
        }
        int poolSize;
        if (!privateNodeHandle.getParam("sss_socket_pool_size", poolSize))
        {
            ROS_ERROR("The parameter sss_socket_pool_size is required.");
            return -1;
        }

        PlaybackChannelFilterNode node(configuration, port, frame_id, poolSize);
        node.run();
    }
    catch (const std::exception& e)
    {
        ROS_ERROR("%s", e.what());
        return -1;
    }

    return 0;
}
