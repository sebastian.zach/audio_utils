#if defined(__unix__) || defined(__linux__)

#include "SocketServer.h"

using namespace introlab;
using namespace std;

SocketServer::SocketServer(int m_port)
{
    m_port = m_port;
    m_socket = socket(AF_INET, SOCK_STREAM, 0);
    if (m_socket == -1)
    {
        THROW_SOCKET_EXCEPTION("Could not create socket", m_socket);
    }

    int optval = 1;
    int error = 0;
    if ((error = setsockopt(m_socket, SOL_SOCKET, SO_REUSEADDR, &optval, sizeof(optval))) < 0)
    {
        THROW_SOCKET_EXCEPTION("Failed to set socket options", error);
    }

    m_address.sin_family = AF_INET;
    m_address.sin_addr.s_addr = INADDR_ANY;
    m_address.sin_port = htons(m_port);

    if ((error = bind(m_socket, (struct sockaddr*)&m_address, sizeof(m_address))) == -1)
    {
        THROW_SOCKET_EXCEPTION("Could not bind to port", error);
    }

    if ((error = listen(m_socket, 5)) < 0)
    {
        THROW_SOCKET_EXCEPTION("Failed to listen for incoming connections", error);
    }

    timeval timeout = {0, 100000};
    if ((error = setsockopt(m_socket, SOL_SOCKET, SO_RCVTIMEO, &timeout, sizeof(timeout))) < 0)
    {
        THROW_SOCKET_EXCEPTION("Failed to set socket timeout", error);
    }
}

SocketServer::~SocketServer()
{
    stop();
}

void SocketServer::start()
{
    if (m_isRunning)
    {
        return;
    }

    m_thread = thread(&SocketServer::_run, this);

    m_isRunning = true;
}

void SocketServer::stop()
{
    if (!m_isRunning)
    {
        return;
    }
    m_isRunning = false;
    if (m_socket > 0)
    {
        shutdown(m_socket, SHUT_RDWR);
        close(m_socket);
        m_socket = 0;
    }
    if (m_thread.joinable())
    {
        m_thread.join();
    }
}

void SocketServer::_run()
{
    cout << "SocketServer listening" << endl;

    int addrlen = sizeof(m_address);

    while (m_isRunning)
    {
        int clientSocket = -2;

        fd_set set;
        FD_ZERO(&set);
        FD_SET(m_socket, &set);

        struct timeval timeout = {0, 100000};

        int error = select(m_socket + 1, &set, nullptr, nullptr, &timeout);
        if (error < 0)
        {
            THROW_SOCKET_EXCEPTION("Failed to select socket", error);
        }
        else if (error == 0)
        {
            continue;  // Timeout
        }

        clientSocket = accept(m_socket, (struct sockaddr*)&m_address, (socklen_t*)&addrlen);

        if (clientSocket < 0)
        {
            if (errno == EWOULDBLOCK || errno == EAGAIN)
            {
                continue;  // Timeout
            }
            else
            {
                THROW_SOCKET_EXCEPTION("Could not accept client", clientSocket);
            }
        }

        try
        {
            _handleClient(clientSocket);
        }
        catch (const SocketException& e)
        {
            cout << "SocketServer: " << e.what() << endl;
        }

        close(clientSocket);
    }
}

#endif
