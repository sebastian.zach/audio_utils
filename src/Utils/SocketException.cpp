#if defined(__unix__) || defined(__linux__)

#include "SocketException.h"

using namespace introlab;
using namespace std;

SocketException::SocketException(const string& message, int errorCode)
    : runtime_error("SocketException: " + message + " (" + to_string(errorCode) + ")")
{
}

SocketException::~SocketException() {}

#endif
