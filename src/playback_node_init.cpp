#include "playback_node.cpp"

int main(int argc, char** argv)
{
    ros::init(argc, argv, "playback_node");

    ros::NodeHandle privateNodeHandle("~");

    PlaybackNodeConfiguration configuration;

    if (!privateNodeHandle.getParam("backend", configuration.backendString))
    {
        ROS_ERROR("The parameter backend must be alsa or pulse_audio.");
        return -1;
    }

    try
    {
        configuration.backend = PcmDevice::parseBackend(configuration.backendString);

        if (!privateNodeHandle.getParam("device", configuration.device))
        {
            ROS_ERROR("The parameter device is required.");
            return -1;
        }
        if (!privateNodeHandle.getParam("format", configuration.formatString))
        {
            ROS_ERROR("The parameter format is required.");
            return -1;
        }
        configuration.format = parseFormat(configuration.formatString);

        if (!privateNodeHandle.getParam("channel_count", configuration.channelCount))
        {
            ROS_ERROR("The parameter channel_count is required.");
            return -1;
        }
        if (!privateNodeHandle.getParam("sampling_frequency", configuration.samplingFrequency))
        {
            ROS_ERROR("The parameter sampling_frequency is required.");
            return -1;
        }
        if (!privateNodeHandle.getParam("frame_sample_count", configuration.frameSampleCount))
        {
            ROS_ERROR("The parameter frame_sample_count is required.");
            return -1;
        }
        if (!privateNodeHandle.getParam("latency_us", configuration.latencyUs))
        {
            ROS_ERROR("The parameter latency_us is required.");
            return -1;
        }

        bool channelMapFound = privateNodeHandle.getParam("channel_map", configuration.channelMap);
        if (channelMapFound && configuration.backend != PcmDevice::Backend::PulseAudio)
        {
            ROS_WARN("The parameter channel_map is only supported with the PulseAudio backend");
        }

        PlaybackNode node(configuration);
        node.run();
    }
    catch (const std::exception& e)
    {
        ROS_ERROR("%s", e.what());
        return -1;
    }

    return 0;
}
