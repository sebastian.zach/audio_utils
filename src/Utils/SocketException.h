#ifndef UTILS_SOCKET_EXCEPTION_H
#define UTILS_SOCKET_EXCEPTION_H

#if defined(__unix__) || defined(__linux__)

#include <stdexcept>

#define THROW_SOCKET_EXCEPTION(message, errorCode)                                                   \
    throw introlab::SocketException((message), (errorCode))

namespace introlab
{
    class SocketException : public std::runtime_error
    {
    public:
        SocketException(const std::string& message, int errorCode);

        ~SocketException() override;
    };
}

#else

#error "Invalid include file"

#endif

#endif
