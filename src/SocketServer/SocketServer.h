#ifndef SOCKET_SERVER_SOCKET_SERVER_H
#define SOCKET_SERVER_SOCKET_SERVER_H

#if defined(__unix__) || defined(__linux__)

#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>
#include <iostream>
#include <thread>
#include "../Utils/SocketException.h"

namespace introlab
{
    class SocketServer
    {
    public:
        SocketServer(int port);
        ~SocketServer();

        void start();
        void stop();


    protected:
        int m_socket;
        sockaddr_in m_address;
        bool m_isRunning = false;
        std::thread m_thread;

        void _run();

        virtual void _handleClient(int clientSocket) = 0;
    };
}

#else

#error "Invalid include file"

#endif

#endif
