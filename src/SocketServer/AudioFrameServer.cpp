#if defined(__unix__) || defined(__linux__)

#include "AudioFrameServer.h"

using namespace introlab;
using namespace std;

std::string nbits_to_format(int nbits)
{
    if (nbits == 8)
    {
        return "signed_8";
    }
    else if (nbits == 16)
    {
        return "signed_16";
    }
    else if (nbits == 32)
    {
        return "signed_32";
    }
    else
    {
        throw std::invalid_argument("Not supported format (nbits=" + std::to_string(nbits) + ")");
    }
}

AudioFrameServer::AudioFrameServer(
    uint port,
    uint nBits,
    uint channelCount,
    uint samplingFrequency,
    uint frameSampleCount,
    string frameId,
    uint poolSize)
    : SocketServer(port),
      m_nBits(nBits),
      m_channelCount(channelCount),
      m_samplingFrequency(samplingFrequency),
      m_frameSampleCount(frameSampleCount),
      m_frameId(frameId),
      m_format(nbits_to_format(nBits)),
      m_threadpool(poolSize)
{
}

AudioFrameServer::~AudioFrameServer()
{
    m_threadpool.join();
}


void AudioFrameServer::_handleClient(int clientSocket)
{
    size_t recvSize = m_nBits / 8 * m_channelCount * m_frameSampleCount;
    vector<uint8_t> buffer(recvSize);


    while (m_isRunning)
    {
        ssize_t numBytesRecv = recv(clientSocket, buffer.data(), recvSize, 0);
        if (numBytesRecv <= 0)
        {
            if (errno != EAGAIN && errno != EWOULDBLOCK)
            {
                THROW_SOCKET_EXCEPTION("Error receiving data from client", errno);
            }
            continue;
        }

        if (numBytesRecv != recvSize)
        {
            // Incomplete frame, discard
            continue;
        }

        if (m_callback != nullptr)
        {
            audio_utils::AudioFrame* frame = new audio_utils::AudioFrame();
            frame->header.stamp = ros::Time::now();

            frame->header.frame_id = m_frameId;
            frame->format = m_format;
            frame->channel_count = m_channelCount;
            frame->sampling_frequency = m_samplingFrequency;
            frame->frame_sample_count = m_frameSampleCount;
            frame->data = buffer;


            boost::shared_ptr<audio_utils::AudioFrame> msg(frame);
            boost::asio::post(m_threadpool, boost::bind(m_callback, msg, m_userdata));
        }
    }
}

void AudioFrameServer::setCallback(void (*_callback)(audio_utils::AudioFramePtr message, void* userdata), void* userdata)
{
    m_callback = _callback;
    m_userdata = userdata;
}

#endif
