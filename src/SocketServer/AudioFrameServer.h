#ifndef SOCKET_SERVER_AUDIO_FRAME_SERVER_H
#define SOCKET_SERVER_AUDIO_FRAME_SERVER_H

#if defined(__unix__) || defined(__linux__)

#include "SocketServer.h"
#include <iostream>
#include <audio_utils/AudioFrame.h>
#include <memory>
#include <boost/asio.hpp>
#include <boost/bind/bind.hpp>
#include <boost/thread.hpp>

namespace introlab
{
    class AudioFrameServer : public SocketServer
    {
    public:
        AudioFrameServer(
            uint port,
            uint nBits,
            uint channelCount,
            uint samplingFrequency,
            uint frameSampleCount,
            std::string frameId,
            uint poolSize = 1);
        ~AudioFrameServer();
        void setCallback(void (*)(audio_utils::AudioFramePtr message, void* userdata), void* userdata);

    private:
        void _handleClient(int clientSocket) override;
        void (*m_callback)(audio_utils::AudioFramePtr message, void* userdata);
        void* m_userdata;
        uint m_nBits;
        uint m_channelCount;
        uint m_frameSampleCount;
        uint m_samplingFrequency;
        std::string m_frameId;
        std::string m_format;
        boost::asio::thread_pool m_threadpool;
    };
}

#else

#error "Invalid include file"

#endif

#endif
